#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");          		// Lizenzinfo
MODULE_AUTHOR("Michael NAPETSCHNG");		// Autor-Informationen
MODULE_DESCRIPTION("Hello World Kernel Module");// Beschreibung
MODULE_VERSION("0.1");         			// Version

// Variablendeklaration inkl. Defaultwert
static char *name = "Kernel";

// Argument/Parameter Variable (charp = char ptr, S_IRUGO Berechtigung [ro])
module_param(name, charp, S_IRUGO);

// Parameterbeschreibung
MODULE_PARM_DESC(name, "Name der in /var/log/kern.log angezeigt werden soll");

// Hello-Funktion
static int __init sayhello_init(void){
   printk(KERN_INFO "Hello %s!\n", name);
   return 0;
}

// Goodbye-Funktion
static void __exit saygoodbye_exit(void){
   printk(KERN_INFO "Goodbye %s!\n", name);
}

// Definition welche Funktion bei einem Module-Load und Unload ausgefuehrt werden soll.
module_init(sayhello_init);
module_exit(saygoodbye_exit);

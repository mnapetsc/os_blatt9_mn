//
//  main.c
//  9_1_Kernel
//
//  Created by Michael Napetschnig on 03.06.19.
//  Copyright © 2019 Michael Napetschnig. All rights reserved.
//

#include <linux/module.h>    /* Needed by all modules */
#include <linux/kernel.h>    /* Needed for KERN_INFO */
#include <linux/init.h>        /* Needed for the macros */

MODULE_LICENSE("GPL");                  // Lizenzinfo
MODULE_AUTHOR("Michael NAPETSCHNG");        // Autor-Informationen
MODULE_DESCRIPTION("Hello World Kernel Module");// Beschreibung
MODULE_VERSION("0.1");                     // Version

// Variablendeklaration inkl. Defaultwert
static char *name = "Kernel";

// Argument/Parameter Variable (charp = char ptr, S_IRUGO Berechtigung [ro])
module_param(name, charp, S_IRUGO);

// Parameterbeschreibung
MODULE_PARM_DESC(name, "Name der in /var/log/kern.log angezeigt werden soll");



int hello_init(void){
    
    printk(KERN_INFO "Hello, %s \n", name);
    return 0;
}

void hello_cleanup(void){
    
    printk(KERN_INFO "Goodbye, %s\n", name);
}

module_init(hello_init);
module_exit(hello_cleanup);
